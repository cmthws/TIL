On Hacker News, I found the following Show HN: "[I've been writing daily TILs for a year](https://news.ycombinator.com/item?id=11068902)". The author, jbranchaud, has been using [his github](https://github.com/jbranchaud/til) to track the bits of knowlege he'd learned each day. Like many others on HN, I thought this was a brilliant idea: 1) document things learned, and 2) to share them in a public fashion so other can learn as well.

This is my repository of "Today I Learned." 
